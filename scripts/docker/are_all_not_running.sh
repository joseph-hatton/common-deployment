#!/usr/bin/env bash

# This script has reverse logic, since we do a while loop on it returning 0.
# Returns 0 if there are NOT $3 copies of $1 running version $2
# Returns 1 if everything is running
# Required information:
#   Argument: $1 as the service name to filter by
#   Argument: $2 as the image version to expect
#   Argument: $3 as the number of replicas to expect
areAllNotRunning () {
  sudo docker stack ps -f "name=$1" -f "desired-state=running" --format "{{.CurrentState}} {{.Image}}" ${STACK_NAME} | awk "/Running/ && /${2}/ { print; }" | wc -l | grep -v $3
  i=$?
  echo "Checking to see if all are running, exit code is $i"
  return $i
}
