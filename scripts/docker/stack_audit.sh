#!/bin/bash
#set -x
stackName=${STACK_NAME:-$1}

# echo "Service=Digest"
for service in $(sudo docker stack services ${stackName} | awk '{print $2}'); do
  if [ "${service}" != "NAME" ]; then
    digest=$(sudo docker service inspect --format '{{.Spec.TaskTemplate.ContainerSpec.Image}}' ${service})
    echo "${service}=${digest}"
  fi
done