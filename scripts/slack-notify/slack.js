#!/usr/bin/env node

// I'm not importing any external dependencies, as to have it run in vanilla js

var SLACK_WEBHOOK_OPTIONS = options = { hostname: 'hooks.slack.com', method: 'POST', path: '/services/T8YHRM04T/BDY2CUHC2/XAkKekhYzNO67eCIWriyzmEf' }
var JENKINS_BUILD_STATUS_SUCCESS = 'SUCCESS'
var JENKINS_BUILD_STATUS_FAILURE = 'FAILURE'

var https = require('https')
var util = require('util')
var url = `${process.env.BUILD_URL}api/json`
console.log(':::: SLACK CODE ::::')
// console.log(`url: ${url}`)

var splits = url.split('/')
var host = splits[2]

// console.log(`host: ${splits[2]}`)
splits.splice(0, 3)
var path = splits.join('/')
// console.log(`path: ${path}`)



var getBuildStatus = () =>
  new Promise((resolve, reject) =>
    https.get({
      host: host,
      path: `/${path}`
    }, (response) => {
      var body = ''
      response.on('data', (d) => body += d)
      response.on('end', () => resolve(JSON.parse(body)))
    })
  )

var buildSlackAttachments = (data) => {
  var slackBody = {
    channel: (process.env.SLACK_CHANNEL) ? process.env.SLACK_CHANNEL : '#fast-alerts',
    username: (process.env.SLACK_USERNAME) ? process.env.SLACK_USERNAME : 'jenkins-bot',
    'icon_emoji': (process.env.SLACK_ICON) ? process.env.SLACK_ICON : ':bust_in_silhouette:',
    "link_names": 1,
    attachments: (attachment) ? [attachment] : []
  }
  var attachment = null
  // console.log(util.inspect(data, { depth: null }))


  var baseText = `<${data.url}|${data.fullDisplayName}>`
  if (data.result === JENKINS_BUILD_STATUS_SUCCESS || data.result === JENKINS_BUILD_STATUS_FAILURE) {
    if (data.result === JENKINS_BUILD_STATUS_SUCCESS) {
      attachment = { color: '#008000', text: `${baseText} has completed successfully.` }
    } else if (data.result === JENKINS_BUILD_STATUS_FAILURE) {
      slackBody.text = '@here'
      attachment = { color: '#ff0000', text: `${baseText} has failed!` }
    }
    if (!process.env.SKIP_WHO && data.changeSet && data.changeSet.items && data.changeSet.items.length > 0) {
      attachment.fields = []
      for (var i = 0; i < data.changeSet.items.length; i++) {
        attachment.fields.push({value: `${data.changeSet.items[i].author.fullName} - ${data.changeSet.items[i].msg}`})
      }
      // console.log('hasChangeSet: attachment.fields', attachment.field)
    } else {
      // console.log((process.env.SKIP_WHO) ? 'skipped who': 'noChangeSet')
    }
  }
  slackBody.attachments = (attachment) ? [attachment] : []

  return slackBody
}

var sendSlackMessage = (slackBody) => {
  if (slackBody.attachments.length > 0) {
    return new Promise((resolve, reject) => {
      var req = https.request(SLACK_WEBHOOK_OPTIONS, (res) => {
        res.setEncoding('utf8')
        res.on('end', () => resolve('slack message sent'))
      })
      req.on('error', (e) => console.log(`problem with request: ${e.message}`))
      req.write(JSON.stringify(slackBody))
      req.end()
      return req
    })
  }
}

return getBuildStatus()
  .then(buildSlackAttachments)
  .then(sendSlackMessage)
