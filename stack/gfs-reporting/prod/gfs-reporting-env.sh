#!/usr/bin/env bash
# sensitive values are all '***'

export DEPLOY_ENV=prod
export DNS_SUFFIX=""
export STACK_NAME=gfs-reporting-prod

export DEPLOY_HOST=stlpstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue
export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export OAUTH_REALM_URL=https://sts.rgare.com/secureauth69
export VAULT_HOST=https://serviceregistry.rgare.net:8201

# All stacks deployed within the nonprod scaffold all get their secrets/configs out of the GFS reporting Prod
export VAULT_USER_ID=StableValue_prod_USER
export VAULT_PATH=StableValue_prod
