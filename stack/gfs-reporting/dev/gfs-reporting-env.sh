#!/usr/bin/env bash
# sensitive values are all '***'

export DEPLOY_ENV=dev
export DNS_SUFFIX="-dev"
export STACK_NAME=gfs-reporting-dev

export DEPLOY_HOST=stldstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue
export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export OAUTH_REALM_URL=https://stsdev.rgare.com/secureauth31
export VAULT_HOST=https://serviceregistry.rgare.net:8201
export GFS_REPORTING_POSTGRES_DIR=/opt/stablevalue/gfs-reporting/dev

# All stacks deployed within the nonprod scaffold all get their secrets/configs out of the GFS reporting dev
export VAULT_USER_ID=StableValue_dev_USER
export VAULT_PATH=StableValue_dev

# Secrets or dynamic variables that need to be set by Jenkins or a separate script:
# export DEPLOY_ENV=dev
# export DOCKER_REGISTRY_PASSWORD=***
# export VAULT_APP_ID=***

#docker stack deploy --compose-file ./docker-compose.yml --with-registry-auth $STACK_NAME
