#!/usr/bin/env bash
set -x

# Required environment variables:
#   VAULT_APP_ID=$VAULT_APP_ID
if [ -z "$VAULT_APP_ID" ]
then
  echo 'Set environment variables before running this script: [VAULT_APP_ID]'
  exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DIR/fast-env.sh"

echo "^ ####################################"
echo "^ Deploying FAST_DOCKER_IMAGE_VERSION version: $FAST_DOCKER_IMAGE_VERSION"

## This script is intended to run from Jenkins/Remotely
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} mkdir -p deploy/${DEPLOY_ENV}/${STACK_NAME}
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} rm deploy/${DEPLOY_ENV}/${STACK_NAME}/*
scp -o StrictHostKeyChecking=no "$DIR/../../../scripts/docker/stack_audit.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../../scripts/docker/are_all_not_running.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/fast-compose.yml" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/fast-env.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../vault-get.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}

ssh -tT -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} << EOSSH
export VAULT_APP_ID=$VAULT_APP_ID
export DOCKER_REGISTRY_HOST=$DOCKER_REGISTRY_HOST
export DEPLOY_ENV=$DEPLOY_ENV
export FAST_DOCKER_IMAGE_VERSION=$FAST_DOCKER_IMAGE_VERSION

cd deploy/$DEPLOY_ENV/${STACK_NAME}
source ./are_all_not_running.sh
source ./fast-env.sh

source ./vault-get.sh \
  DOCKER_REGISTRY_USER \
  DOCKER_REGISTRY_PASSWORD \
  FAST_DEV_AUTH_CLIENT_ID

sudo docker login -u \$DOCKER_REGISTRY_USER -p \$DOCKER_REGISTRY_PASSWORD \$DOCKER_REGISTRY_HOST || exit 1

export AUDIT_TIMESTAMP=\$(date +%s)
export AUDIT_BEFORE=audit-\${AUDIT_TIMESTAMP}-before.txt
export AUDIT_AFTER=audit-\${AUDIT_TIMESTAMP}-after.txt

echo "====BEFORE===="
sudo -E ./stack_audit.sh | sort
sudo -E ./stack_audit.sh | sort > \${AUDIT_BEFORE}

sudo -E docker stack deploy --compose-file ./fast-compose.yml --with-registry-auth \$STACK_NAME || exit 2

echo "^ stack deployed, waiting for containers to be up..."
sleep 10
while areAllNotRunning ${STACK_NAME} $FAST_DOCKER_IMAGE_VERSION 2 ; do
  echo "waiting for exit code to not be 0, meaning all FAST containers are running the expected version $FAST_DOCKER_IMAGE_VERSION"
  ((c++)) && ((c==240)) && c=0 && echo 'exiting early, deploy took longer than expected' && exit 5
  sleep 2
done
echo "all expected running containers are running"

echo "====AFTER===="
sudo -E ./stack_audit.sh | sort > \${AUDIT_AFTER}
sudo -E ./stack_audit.sh | sort

set +e
echo "====DIFFERENCE===="
diff \${AUDIT_BEFORE} \${AUDIT_AFTER}

exit 0

EOSSH
