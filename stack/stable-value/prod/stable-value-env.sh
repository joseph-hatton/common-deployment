#!/usr/bin/env bash
# sensitive values are all '***'

export DEPLOY_ENV=prod
export DNS_SUFFIX=""
export STACK_NAME=stable-value-prod
export GRAILS_ENV=prod

export DEPLOY_HOST=stlpstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue

export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export OAUTH_REALM_URL=https://sts.rgare.com/secureauth69
export VAULT_HOST=https://serviceregistry.rgare.net:8201

#export OLD_APP_BASE_URL=http://stldstbvalapp01.rgare.net:8085/stableValue
export OLD_APP_BASE_URL=http://legacy_api:8080/stableValue

# All stacks deployed within the nonprod scaffold all get their secrets/configs out of the stable value dev
export VAULT_USER_ID=StableValue_prod_USER
export VAULT_PATH=StableValue_prod

# Secrets or dynamic variables that need to be set by Jenkins or a separate script:
# export DEPLOY_ENV=dev
# export DOCKER_REGISTRY_PASSWORD=***
# export VAULT_APP_ID=***

#docker stack deploy --compose-file ./docker-compose.yml --with-registry-auth $STACK_NAME