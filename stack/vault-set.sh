#!/usr/bin/env bash

# usage: './vault-set VaultKey_1=Secret_1 VaultKey_2=Secret_2 VaultKey_X=Secret_X'

if [ -z "$VAULT_APP_ID" ] || [ -z "$VAULT_HOST" ] || [ -z "$VAULT_USER_ID" ] || [ -z "$VAULT_PATH" ]
then
  echo 'Set vault environment variables before running this script: [VAULT_APP_ID, VAULT_HOST, VAULT_USER_ID, VAULT_PATH]'
  exit 1
fi

echo "authenticating with vault"
LOGIN_RES=$(curl "$VAULT_HOST/v1/auth/app-id/login" -fskd "{\"app_id\":\"$VAULT_APP_ID\",\"user_id\":\"$VAULT_USER_ID\"}" -H 'Content-Type: application/json')

if [ "$?" != "0" ]
then
  echo 'could not authenticate with vault, exiting'
  exit 1
fi

CLIENT_TOKEN=$(echo $LOGIN_RES | jq -r .auth.client_token)

if [ "$?" != "0" ]
then
  echo 'check to make sure jq is installed properly, exiting'
  exit 1
fi

for VAR in "$@"
do
  IFS==
  set -- $VAR

  if [ -n "$2" ]
  then
   echo "setting $1 in vault"
   PROP_RES=$(curl "$VAULT_HOST/v1/secret/$VAULT_PATH/$1" -fsk -d "{\"value\": \"$2\"}" -H "X-Vault-Token: $CLIENT_TOKEN" -H 'Content-Type: application/json')

   if [ "$?" != "0" ]
   then
     echo 'could not set value in vault, exiting'
     exit 1
   fi

  fi
done
