export DEPLOYMENT_TIME=$(date)
export STACK_NAME=monitor
export DEPLOY_ENV=prod
export DEPLOY_HOST=stlpstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue
export DNS_SUFFIX=-np
export OAUTH_REALM_URL=https://sts.rgare.com/secureauth69
export OAUTH_AUTH_URL=$OAUTH_REALM_URL/secureauth.aspx
export OAUTH_TOKEN_URL=$OAUTH_REALM_URL/oidctoken.aspx
export OAUTH_USER_URL=$OAUTH_REALM_URL/oidcuserinfo.aspx

export VAULT_HOST=https://serviceregistry.rgare.net:8201
export VAULT_USER_ID=StableValue_prod_USER
export VAULT_PATH=StableValue_prod

export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export DOCKER_LOCATION=/bin/docker
export DOCKER_LIB=/docker/

export SLACK_URL=https://hooks.slack.com/services/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
export SLACK_CHANNEL=prod-devops-alerts
export SLACK_USER=np-alertmanager

export PROMETHEUS_VOLUME=/opt/stablevalue/monitoring/swarmprom/prometheus
export GRAFANA_VOLUME=/opt/stablevalue/monitoring/swarmprom/grafana
export ALERTMANAGER_VOLUME=/opt/stablevalue/monitoring/swarmprom/alertmanager

export INFLUX_API_VERSION=dev