#!/usr/bin/env bash
set -ex

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"

source "$DIR/monitoring-env.sh"

DEPLOY_FOLDER=deploy/${DEPLOY_ENV}/${STACK_NAME}

ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} mkdir -p $DEPLOY_FOLDER
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} rm -rf ${DEPLOY_FOLDER}/*
scp -ro StrictHostKeyChecking=no * ${DEPLOY_USER}@${DEPLOY_HOST}:${DEPLOY_FOLDER}/
scp -o StrictHostKeyChecking=no ../../../scripts/docker/stack_audit.sh ${DEPLOY_USER}@${DEPLOY_HOST}:${DEPLOY_FOLDER}
scp -o StrictHostKeyChecking=no ../../vault-get.sh ${DEPLOY_USER}@${DEPLOY_HOST}:${DEPLOY_FOLDER}

ssh -t -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} << EOSSH
set -e
set +x
export VAULT_APP_ID=$VAULT_APP_ID
export DOCKER_REGISTRY_USER=$DOCKER_REGISTRY_USER
export DOCKER_REGISTRY_PASSWORD=$DOCKER_REGISTRY_PASSWORD
export DEPLOY_ENV=$DEPLOY_ENV
cd ${DEPLOY_FOLDER}

pwd
ls

source ./monitoring-env.sh

source ./vault-get.sh  \
  DOCKER_REGISTRY_USER \
  DOCKER_REGISTRY_PASSWORD \
  GRAFANA_CLIENT_ID \
  GRAFANA_CLIENT_SECRET \
  INFLUX_API_USER \
  INFLUX_API_PASSWORD

# sudo docker login -u $DOCKER_REGISTRY_USER -p "$DOCKER_REGISTRY_PASSWORD" \$DOCKER_REGISTRY_HOST

export AUDIT_TIMESTAMP=\$(date +%s)
export AUDIT_BEFORE=audit-\${AUDIT_TIMESTAMP}-before.txt
export AUDIT_AFTER=audit-\${AUDIT_TIMESTAMP}-after.txt

echo "====BEFORE===="
sudo -E ./stack_audit.sh | sort
sudo -E ./stack_audit.sh | sort > \${AUDIT_BEFORE}

sudo -E docker stack deploy --compose-file ./monitoring-compose.yml --with-registry-auth \$STACK_NAME

echo "====AFTER===="
sudo -E ./stack_audit.sh | sort > \${AUDIT_AFTER}
sudo -E ./stack_audit.sh | sort

set +e
echo "====DIFFERENCE===="
diff \${AUDIT_BEFORE} \${AUDIT_AFTER}

exit 0
EOSSH
