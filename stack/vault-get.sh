#!/usr/bin/env bash

# usage: 'source ./vault-get ENV_1=VaultKey_1 ENV_2=VaultKey_2 ENV_X=VaultKey_X'
# note: if the env name and key are the same, just use ./vault-get ENV_1

if [ -z "$VAULT_APP_ID" ] || [ -z "$VAULT_HOST" ] || [ -z "$VAULT_USER_ID" ] || [ -z "$VAULT_PATH" ]
then
  echo 'Set vault environment variables before running this script: [VAULT_APP_ID, VAULT_HOST, VAULT_USER_ID, VAULT_PATH]'
  exit 1
fi

echo "authenticating with vault"
LOGIN_RES=$(curl "$VAULT_HOST/v1/auth/app-id/login" -fskd "{\"app_id\":\"$VAULT_APP_ID\",\"user_id\":\"$VAULT_USER_ID\"}" -H 'Content-Type: application/json')

if [ "$?" != "0" ]
then
  echo 'could not authenticate with vault, exiting'
  exit 1
fi

CLIENT_TOKEN=$(echo $LOGIN_RES | jq -r .auth.client_token)

if [ "$?" != "0" ]
then
  echo 'check to make sure jq is installed properly, exiting'
  exit 1
fi

for VAR in "$@"
do
  IFS==
  set -- $VAR
  function exportValue {
   URL="$VAULT_HOST/v1/secret/$VAULT_PATH/$2"
   echo "^ attempting to vault-get $URL"
   PROP_RES=$(curl "$VAULT_HOST/v1/secret/$VAULT_PATH/$2" -fsk -H "X-Vault-Token: $CLIENT_TOKEN")
   if [ "$?" != "0" ]
   then
     echo 'could not get value from vault, exiting'
     exit 1
   fi
   VALUE=$(echo "$PROP_RES" | jq -r .data.value)
   export "$1"="$VALUE"
  }

  if [ -n "$2" ]
  then
   echo "exporting $2 as $1"
   exportValue $1 $2

  else
   echo "exporting $1"
   exportValue $1 $1

  fi
done
