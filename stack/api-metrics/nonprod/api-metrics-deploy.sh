#!/usr/bin/env bash
set -x

# Required environment variables:
#   VAULT_APP_ID=$VAULT_APP_ID
if [ -z "$VAULT_APP_ID" ]
then
  echo 'Set environment variables before running this script: [VAULT_APP_ID]'
  exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DIR/api-metrics-env.sh"

echo "^ ####################################"

## This script is intended to run from Jenkins/Remotely
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} mkdir -p deploy/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../../scripts/docker/are_all_not_running.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../../scripts/docker/stack_audit.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/api-metrics-compose.yml" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/api-metrics-env.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../vault-get.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${STACK_NAME}

ssh -tT -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} << EOSSH
export VAULT_APP_ID=$VAULT_APP_ID
export DOCKER_REGISTRY_HOST=$DOCKER_REGISTRY_HOST

cd deploy/${STACK_NAME}
source ./api-metrics-env.sh

source ./vault-get.sh \
  DOCKER_REGISTRY_USER \
  DOCKER_REGISTRY_PASSWORD \
  STABLE_VALUE_DEV_PASSWORD_FLOW_AUTH_CLIENT_ID \
  STABLE_VALUE_DEV_PASSWORD_FLOW_AUTH_CLIENT_SECRET \
  STABLE_VALUE_TEST_PASSWORD_FLOW_AUTH_CLIENT_ID \
  STABLE_VALUE_TEST_PASSWORD_FLOW_AUTH_CLIENT_SECRET \
  STABLE_VALUE_UAT_PASSWORD_FLOW_AUTH_CLIENT_ID \
  STABLE_VALUE_UAT_PASSWORD_FLOW_AUTH_CLIENT_SECRET \
  API_TESTING_USER \
  API_TESTING_USER_PASSWORD

sudo docker login -u \$DOCKER_REGISTRY_USER -p \$DOCKER_REGISTRY_PASSWORD \$DOCKER_REGISTRY_HOST || exit 1

sudo -E docker stack deploy --compose-file ./api-metrics-compose.yml --with-registry-auth \$STACK_NAME || exit 2

exit 0
EOSSH
