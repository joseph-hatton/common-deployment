#!/usr/bin/env bash
export STACK_NAME=api-metrics

export DEPLOY_HOST=stlpstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue

export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443

export VAULT_HOST=https://serviceregistry.rgare.net:8201
export VAULT_USER_ID=StableValue_prod_USER
export VAULT_PATH=StableValue_prod