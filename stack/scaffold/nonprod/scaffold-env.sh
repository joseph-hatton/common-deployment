#!/usr/bin/env bash

export DEPLOY_ENV=nonprod
export DNS_SUFFIX="-dev"
export ROUTER_VERSION=0.8.14
export STACK_NAME=scaffold

export DEPLOY_HOST=stldstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue
export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export VAULT_HOST=https://serviceregistry.rgare.net:8201
export VAULT_USER_ID=StableValue_dev_USER
export VAULT_PATH=StableValue_dev

export REFRESH_TOKEN_STALE_TIME_HOURS=120

export DAPPER_VERSION=dev

# Secrets that need to be set by Jenkins or a separate script.
# export DOCKER_REGISTRY_PASSWORD=***
# export VAULT_APP_ID=***
# export AUTH_CLIENT_ID=***
# export AUTH_CLIENT_SECRET=***