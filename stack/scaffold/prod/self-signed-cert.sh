#!/usr/bin/env bash
set -x

CERT_COUNT=$(sudo docker secret ls -f "Name=cert.pem" --format "{{.Name}}" | wc -l)
KEY_COUNT=$(sudo docker secret ls -f "Name=key.pem" --format "{{.Name}}" | wc -l)

if [ $CERT_COUNT == 0 ] || [ $KEY_COUNT == 0 ]
then
  if [ $CERT_COUNT == 1 ]
  then
    sudo docker secret rm cert.pem
  fi

  if [ $KEY_COUNT == 1 ]
  then
    sudo docker secret rm key.pem
  fi

  echo 'Some combination of the cert and the key are not in docker secrets. Regenerating and placing them there...'
  
  openssl genrsa -des3 -passout pass:x -out server.pass.key 2048
  openssl rsa -passin pass:x -in server.pass.key -out key.pem
  rm server.pass.key
  openssl req -new -key key.pem -out server.csr \
    -subj "/C=US/ST=Missouri/L=Chesterfield/O=RGA/OU=Actuarial Solutions/CN=stldstbvldock01.rgare.net"
  openssl x509 -req -days 3650 -in server.csr -signkey key.pem -out cert.pem

  sudo docker secret create cert.pem cert.pem
  sudo docker secret create key.pem key.pem

  rm cert.pem key.pem server.csr
fi



