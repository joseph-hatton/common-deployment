#!/usr/bin/env bash
set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "$DIR/scaffold-env.sh"

## This script is intended to run from Jenkins/Remotely
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} mkdir -p deploy/${DEPLOY_ENV}/${STACK_NAME}
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} rm deploy/${DEPLOY_ENV}/${STACK_NAME}/*
scp -o StrictHostKeyChecking=no "$DIR/networks-public.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/scaffold-env.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/scaffold-compose.yml" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/self-signed-cert.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../vault-get.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}

ssh -tT -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} << EOSSH
export VAULT_APP_ID=$VAULT_APP_ID
export DEPLOY_ENV=$DEPLOY_ENV

cd deploy/${DEPLOY_ENV}/${STACK_NAME}

source ./scaffold-env.sh
source ./vault-get.sh \
  DOCKER_REGISTRY_USER \
  DOCKER_REGISTRY_PASSWORD \
  FAST_PROD_AUTH_CLIENT_SECRET \
  STABLE_VALUE_PROD_AUTH_CLIENT_SECRET \
  GFS_REPORTING_PROD_AUTH_CLIENT_SECRET \
  LDAP_USER \
  LDAP_PASSWORD

if [ -z \$DOCKER_REGISTRY_USER ] || [ -z \$DOCKER_REGISTRY_PASSWORD ] || [ -z \$FAST_PROD_AUTH_CLIENT_SECRET ]
then
  echo 'Set vault secrets before running this script: [DOCKER_REGISTRY_USER, DOCKER_REGISTRY_PASSWORD, FAST_PROD_AUTH_CLIENT_SECRET]'
  exit 1
fi

echo 'Running self-signed-cert.sh'
./self-signed-cert.sh
./networks-public.sh
sudo -E docker stack deploy --compose-file ./scaffold-compose.yml --with-registry-auth \$STACK_NAME
EOSSH
