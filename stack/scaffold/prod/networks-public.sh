# create the networks for the environments that will be sharing this swarm.
set -e
sudo docker network create --driver=overlay --opt=encrypted --attachable=true public || true
sudo docker network create --driver=overlay --opt=encrypted --attachable=true private_shared || true
