#!/usr/bin/env bash
set -x

# Required environment variables:
#   VAULT_APP_ID=$VAULT_APP_ID
if [ -z "$VAULT_APP_ID" ]
then
  echo 'Set environment variables before running this script: [VAULT_APP_ID]'
  exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

API_CONTAINER_NAME="gfs-pricing-${DEPLOY_ENV}_api"
UI_CONTAINER_NAME="gfs-pricing-${DEPLOY_ENV}_ui"
SP_CONTAINER_NAME="gfs-marketing-sp-${DEPLOY_ENV}_api"
source "$DIR/gfs-pricing-env.sh"

source "$DIR/../../../scripts/docker/get_remote_deployed_service_tags" ${DEPLOY_USER}@${DEPLOY_HOST} $API_CONTAINER_NAME $UI_CONTAINER_NAME $SP_CONTAINER_NAME

if [ -z "$API_VERSION" ]
then
  echo "^ API version not specifed, defaulting to deployed version"
  API_VERSION_KEY="${API_CONTAINER_NAME//-/_}_VERSION"
  export API_VERSION="${!API_VERSION_KEY}"
fi

if [ -z "$UI_VERSION" ] 
then
  echo "^ UI version not specifed, defaulting to deployed version"
  UI_VERSION_KEY="${UI_CONTAINER_NAME//-/_}_VERSION"
  export UI_VERSION="${!UI_VERSION_KEY}"
fi

if [ -z "$SP_VERSION" ] 
then
  echo "^ SP version not specifed, defaulting to deployed version"
  SP_VERSION_KEY="${SP_CONTAINER_NAME//-/_}_VERSION"
  export SP_VERSION="${!SP_VERSION_KEY}"
fi

echo "^ ####################################"
echo "^ Deploying API        version: $API_VERSION"
echo "^ Deploying UI         version: $UI_VERSION"
echo "^ Deploying SP         version: $SP_VERSION"

echo "API_VERSION=$API_VERSION" > "${WORKSPACE}/versions.properties"
echo "UI_VERSION=$UI_VERSION" >> "${WORKSPACE}/versions.properties"
echo "SP_VERSION=$SP_VERSION" >> "${WORKSPACE}/versions.properties"

## This script is intended to run from Jenkins/Remotely
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} mkdir -p deploy/${DEPLOY_ENV}/${STACK_NAME}
ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} rm deploy/${DEPLOY_ENV}/${STACK_NAME}/*
scp -o StrictHostKeyChecking=no "$DIR/../../../scripts/docker/are_all_not_running.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../../scripts/docker/stack_audit.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/gfs-pricing-compose.yml" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/gfs-pricing-env.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}
scp -o StrictHostKeyChecking=no "$DIR/../../vault-get.sh" ${DEPLOY_USER}@${DEPLOY_HOST}:deploy/${DEPLOY_ENV}/${STACK_NAME}

ssh -tT -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_HOST} << EOSSH
export VAULT_APP_ID=$VAULT_APP_ID
export DOCKER_REGISTRY_HOST=$DOCKER_REGISTRY_HOST
export DEPLOY_ENV=$DEPLOY_ENV
export API_VERSION=$API_VERSION
export UI_VERSION=$UI_VERSION
export SP_VERSION=$SP_VERSION

cd deploy/$DEPLOY_ENV/${STACK_NAME}
source ./are_all_not_running.sh
source ./gfs-pricing-env.sh

source ./vault-get.sh \
  DOCKER_REGISTRY_USER \
  DOCKER_REGISTRY_PASSWORD \
  GFS_PRICING_API_UAT_POSTGRES_USER \
  GFS_PRICING_API_UAT_POSTGRES_PASSWORD \
  GFS_PRICING_API_UAT_FLYWAY_USER \
  GFS_PRICING_API_UAT_FLYWAY_PASSWORD \
  GFS_MARKETING_SP_API_UAT_CLIENT_ID \
  GFS_MARKETING_SP_API_UAT_CLIENT_SECRET \
  GFS_PRICING_UAT_AUTH_CLIENT_ID

sudo docker login -u \$DOCKER_REGISTRY_USER -p \$DOCKER_REGISTRY_PASSWORD \$DOCKER_REGISTRY_HOST || exit 1

export AUDIT_TIMESTAMP=\$(date +%s)
export AUDIT_BEFORE=audit-\${AUDIT_TIMESTAMP}-before.txt
export AUDIT_AFTER=audit-\${AUDIT_TIMESTAMP}-after.txt

echo "====BEFORE===="
sudo -E ./stack_audit.sh | sort
sudo -E ./stack_audit.sh | sort > \${AUDIT_BEFORE}

sudo -E docker stack deploy --compose-file ./gfs-pricing-compose.yml --with-registry-auth \$STACK_NAME || exit 2

echo "^ stack deployed, waiting for containers to be up..."
sleep 10
while areAllNotRunning ${STACK_NAME}_api $API_VERSION 2 ; do
  echo "waiting for exit code to not be 0, meaning all API containers are running the expected version $API_VERSION"
  ((c++)) && ((c==120)) && c=0 && exit 5
  sleep 2
done

while areAllNotRunning ${STACK_NAME}_ui $UI_VERSION 2 ; do
  echo "waiting for exit code to not be 0, meaning all UI containers are running the expected version $UI_VERSION"
  ((c++)) && ((c==120)) && c=0 && echo 'exiting early, deploy took longer than expected' && exit 5
  sleep 2
done

while areAllNotRunning ${STACK_NAME}_sp $SP_VERSION 1 ; do
  echo "waiting for exit code to not be 0, meaning all ${STACK_NAME}_sp containers are running the expected version $SP_VERSION"
  ((c++)) && ((c==120)) && c=0 && exit 5
  sleep 2
done
echo "all expected running containers are running"

echo "====AFTER===="
sudo -E ./stack_audit.sh | sort > \${AUDIT_AFTER}
sudo -E ./stack_audit.sh | sort

set +e
echo "====DIFFERENCE===="
diff \${AUDIT_BEFORE} \${AUDIT_AFTER}

exit 0
EOSSH
