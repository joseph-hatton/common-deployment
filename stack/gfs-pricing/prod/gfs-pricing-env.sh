#!/usr/bin/env bash
# sensitive values are all '***'

export DEPLOY_ENV=prod
export DNS_SUFFIX="-prod"
export STACK_NAME=gfs-pricing-prod

export DEPLOY_HOST=stldstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue
export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export OAUTH_REALM_URL=https://sts.rgare.com/secureauth69
export VAULT_HOST=https://serviceregistry.rgare.net:8201
export GFS_PRICING_POSTGRES_DIR=/opt/stablevalue/gfs-pricing/prod

# All stacks deployed within the nonprod scaffold all get their secrets/configs out of the GFS Pricing Prod
export VAULT_USER_ID=StableValue_prod_USER
export VAULT_PATH=StableValue_prod

#docker stack deploy --compose-file ./docker-compose.yml --with-registry-auth $STACK_NAME
