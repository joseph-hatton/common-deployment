#!/usr/bin/env bash
# sensitive values are all '***'

export DEPLOY_ENV=test
export DNS_SUFFIX="-test"
export STACK_NAME=gfs-pricing-test

export DEPLOY_HOST=stldstbvldock01.rgare.net
export DEPLOY_USER=svc4stablevalue
export DOCKER_REGISTRY_HOST=stlpartifact01.rgare.net:5443
export DOCKER_SOCKET=/docker/docker.sock
export OAUTH_REALM_URL=https://stsdev.rgare.com/secureauth31
export VAULT_HOST=https://serviceregistry.rgare.net:8201
export GFS_PRICING_POSTGRES_DIR=/opt/stablevalue/gfs-pricing/test

# All stacks deployed within the nonprod scaffold all get their secrets/configs out of the GFS Pricing dev
export VAULT_USER_ID=StableValue_dev_USER
export VAULT_PATH=StableValue_dev

#docker stack deploy --compose-file ./docker-compose.yml --with-registry-auth $STACK_NAME
