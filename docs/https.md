# HTTPS

HTTPS is set up adding TLS certificate (chain) and key to a server to allow it to server secure traffic and is generally over the standard 443 port.

The certificate (chain) and key could be directly in your application, on your application server or a proxy in front of your app.

---

## Self-Signed vs Signed Certificate from a Certificate Authority

Both types function the same, but modern day browsers will tell the users that sites using `Self-Signed` Certificates are not secure.

Generally, you don't want to use `Self-Signed` Certificates on any user-facing urls.

---

## Example request workflow

* Someone tries to go to `https://stable-value-dev.rgare.net`
* That configuration is in RGA's F5 Load Balancer
  * It is configured to:
    * Upgrade any HTTP (port 80) traffic to HTTPS (port 443)
    * Terminate TLS for their `*.rgare.net`'s Wildcard Signed Certificate (wildcard because it works with all subdomains)
    * Re-encrypt TLS and send the request to one of the `healthy` Docker Swarm Nodes [on port 443](stldstbvldock01.rgare.net, stldstbvldock03.rgare.net, stldstbvldock03.rgare.net)
    * The Swarm Router is what is is exposed on those nodes on port 443 and has a self-signed certificate configured (since this url is not customer facing, the f5 url is)

--- 

## Swarm Router Self-Signed Certificate

Everytime a scaffold (prod and nonprod is deployed) it runs a shell script called [self-signed-cert.sh](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fscaffold%2Fnonprod%2Fself-signed-cert.sh&version=GBmaster&_a=contents) that checks to see if there is a self-signed certificate and key in Docker Secrets.

If there is no key.pem and cert.pem (or it is missing one of them), it deletes both secrets from docker and regenerates a new cert.pem and key.pem and stores them back into Docker secrets under those same names.  

They Docker Secrets are referenced in the [Swarm Router compose.yml](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fscaffold%2Fnonprod%2Fscaffold-compose.yml&version=GBmaster&_a=contents).  

* At the very bottom there is a secrets section that states that cert.pem and key.pem Docker Secrets are created externally and that they shouldn't be created via the Docker Stack deployment.
* They are also defined under the `router:` section's `secrets:` section.  
* Docker Secrets basically become flat files (by the same name as the Docker Secret) under /run/secrets/ in the Docker Container.
* The Swarm Router already looks for `/var/run/secrets/key.pem` and `/var/run/secrets/cert.pem` before anywhere else.