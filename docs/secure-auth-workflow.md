# Secure Auth Workflow

[General SecureAuth (OAuth) Info](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%secure-auth-info.md&version=GBmaster&_a=contents)

[SecureAuth with SwarmRouter Setup](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%secure-auth-config.md&version=GBmaster&_a=contents)

---

## API Workflow

_*Note: * Assuming you have a working SwarmRouter setup_

### An unauthenticated API request

* an HTTP request with a missing or invalid `--Authorization: Bearer {TOKEN}` header goes to https://stable-value-dev-api.rgare.net/api/v1/some-api
* The F5 load balancer with `stable-value-dev-api.rgare.net` configured, proxies the request to the `healthy` Docker Swarm Nodes it knows about (stldstbvldock01.rgare.net, stldstbvldock02.rgare.net, stldstbvldock03.rgare.net)
* The Docker Swarm `Mesh Network` sees the HTTPS traffic for port 443 and sends it to the only Docker Service running with that port, the [Swarm Router (look for router: ports: 443:8443)](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fscaffold%2Fnonprod%2Fscaffold-compose.yml&version=GBmaster&_a=contents).
* The Swarm Router matches the requested URL with the configured Docker Services it knows about (with the `labels`)
  * It finds the stable-value-dev-api with label `ingress.dnsname` of `stable-value-api-dev.rgare.net`
  * It knows to use `api` auth, because of the `ingress.auth` label
  * The `ingress.config.realm` must be correct for the Client ID and Client Secret you are trying to use.
  * The `ingress.config.clientId` must match on eof the Client ID/Client Secret pairs configured in the Swarm Router compose.yml.
  * The `ingress.targetport` is used to know which port to proxy on the backend (443 (router) > `ingress.targetport` (docker service)).
* Being configured as `API` via the label `ingress.auth`, the router just flatly rejects the request that's not authorized.  
  * This is the expected behavior as the calling client should be able to maintain a valid token (and know when to get a new one).

---

### An authenticated API request

* an HTTP request with a valid `--Authorization: Bearer {TOKEN}` header goes to https://stable-value-dev-api.rgare.net/api/v1/some-api
* The F5 load balancer with `stable-value-dev-api.rgare.net` configured, proxies the request to the `healthy` Docker Swarm Nodes it knows about (stldstbvldock01.rgare.net, stldstbvldock02.rgare.net, stldstbvldock03.rgare.net)
* The Docker Swarm `Mesh Network` sees the HTTPS traffic for port 443 and sends it to the only Docker Service running with that port, the [Swarm Router (look for router: ports: 443:8443)](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fscaffold%2Fnonprod%2Fscaffold-compose.yml&version=GBmaster&_a=contents).
* The Swarm Router matches the requested URL with the configured Docker Services it knows about (with the `labels`)
* Being configured as `API` via the label `ingress.auth`, the router needs to validate the token
  * It submits a POST request to SecureAuth directly with the Docker Service's Client ID, Client Secret and the current user token
    * If valid, the response from SecureAuth will contain how long the token is good for and information about the user.
    * The router caches this in its Redis database
    * This is *most* of the information seen via the router utility url /__auth/current-user
    * The router then proxies the request to the backend Docker Service and the user sees the app they expect
    * If the token is invalid, the Swarm Router returns unauthorized to the client

---

_*Note: * UI requests are handled differently.  Since that means the users are on a browser, the Swarm Router can send an HTTP redirect to SecureAuth's login page when the user is no longer logged in._

### An unauthenticated UI request

* an HTTP request with a missing or invalid `Swarm Router` specific cookie with a primary key of the `Swarm Router`'s redis database goes to https://stable-value-dev-api.rgare.net/api/v1/some-api
* The F5 load balancer with `stable-value-dev.rgare.net` configured, proxies the request to the `healthy` Docker Swarm Nodes it knows about (stldstbvldock01.rgare.net, stldstbvldock02.rgare.net, stldstbvldock03.rgare.net)
* The Docker Swarm `Mesh Network` sees the HTTPS traffic for port 443 and sends it to the only Docker Service running with that port, the [Swarm Router (look for router: ports: 443:8443)](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fscaffold%2Fnonprod%2Fscaffold-compose.yml&version=GBmaster&_a=contents).
* The Swarm Router matches the requested URL with the configured Docker Services it knows about (with the `labels`)
  * It finds the stable-value-dev with label `ingress.dnsname` of `stable-value-dev.rgare.net`
  * It knows to use `api+ui` auth, because of the `ingress.auth` label
  * The `ingress.config.realm` must be correct for the Client ID and Client Secret you are trying to use.
  * The `ingress.config.clientId` must match on eof the Client ID/Client Secret pairs configured in the Swarm Router compose.yml.
  * The `ingress.targetport` is used to know which port to proxy on the backend (443 (router) > `ingress.targetport` (docker service)).
* Being configured as `api+ui` via the label `ingress.auth`, the router knows to send an HTTP redirect to the user since they are not authenticated
  * Part of the login url that the `Swarm Router` responds with is the redirect URL to tell SecureAuth where to send the user back to in the application.
  * This `redirect url` MUST be configured on the UI's specific client ID in SecureAuth or it will consider it a suspicious redirect and will prevent it from happening.
  * Once the user logs into SecureAuth, the user is redirected back to the application specified by the `redirect url`
    * This `redirect url` is a specific Swarm Router endpoint that is automatically on every `ui` auth enabled application at `{your app url}/__auth/receive-auth-code`
    * The Swarm Router receives the auth code, stores everything it just received into its Redis database with a new `sess:` prefixed key. 
    * Then returns the user back to where they were and gives them an HTTP cookie where the value is the `sess:` prefixed key (to tie the browser session to a specificly authenticated SecureAuth user and access token.)

---

### Making a request to a SecureAuth protected API from a SecureAuth protected UI

In order to call any SecureAuth protected API, you must add a `--Authorization: Bearer {aSecureAuthToken}` header to all of your API calls.

Normally, you would have to handle all of that logic on your own in javascript to keep the SecureAuth token valid and know when to get a knew one from SecureAuth.

Luckily for us, the `SwarmRouter` does this work for us and maintains that token in its Redis database.  To maintain the link between the Redis database entry and the specific user, a cookie is sent back to the user to be maintained in the browser.  This cookie is part of the key that is prefixed with `sess:{cookie value here}` in Redis.  

But, we don't have to worry about Redis or specific database rows, thanks to the `SwarmRouter`.

The router automatically proxies our applications with a few Docker Swarm-specific end points for `UI` auth Docker Services.
* `{your app url}/__auth/receive-auth-code` is added to be able to handle a redirect URL from SecureAuth after a valid UI login.  
  * _This is the REDIRECT url that needs to be configured in SecureAuth for your UI to work properly._
  * If your Docker Service Label `ingress.dnsname` is `stable-value-dev.rgare.net` (and assuming HTTPS forced), then you would need the SecureAuth team to add `https://stable-value-dev.rgare.net/__auth/receive-auth-code` as a redirect url.  
  * Note: you can add multiple redirect urls.
* `{your app url}/__auth/current-user` is added to give you access to all information returned about the user from SecureAuth, _EXCEPT THE REFRESH TOKEN_.  
  * Except, the refresh token.  Those are always kept private from the user and are used in the backend to refresh the 1 hour access tokens that are given to the user.
  * It does contain the `access token` which is the value to be used to send to the API in the `Authorization` header.
  * It also contains all groups returned from SecureAuth.

The easiest way to to call your SecureAuth protected APIs is to:

* first check to see if you cached a user and token locally, before every API request
* if not, call `{your app url}/__auth/current-user`, 
  * cache response as your cached user and token (to prevent calling the actual endpoint on each request)
  * if the call completely fails, then consider that as the user being fully logged out.  
* grab the access token value and set it as the `Authorization` header with value `Bearer {access token}`
* then make the api call



