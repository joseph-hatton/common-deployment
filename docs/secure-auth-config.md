# How we enable SecureAuth for our apps using Swarm Router

We run a `Swarm Router` to enable SecureAuth proxying over specific Docker `Services` in our `Docker Swarm`.

The `Swarm Router` is an in-house project under Actuarial Development.

[Documentation on the Swarm Router can be found](https://tfs.rgare.net/Actuarial%20Solutions/Actuarial%20Development/_git/swarm-router)

---

## Configuration

We had Client IDs and Client Secrets generated for each of our UI applications (including their redirect URLs).

### Setting Client IDs and Client Secrets to the Swarm Router

The Client IDs and Client Secrets are added to the environment of the router in:
[Scaffold NonProd Compose.yml](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fscaffold%2Fnonprod%2Fscaffold-compose.yml&version=GBmaster&_a=contents)

The router looks for all environment variables that start with `AUTH_CLIENT_SECRET_`.  The rest of the environment variable name is the Client ID and the value is the Client Secret.  The client secrets are not hard-coded in the Yaml, but passed in as environment variables themselves (from Vault).

For instance: `AUTH_CLIENT_SECRET_b9776287477e4f6e85f7625a4ca9580d: ${STABLE_VALUE_DEV_AUTH_CLIENT_SECRET}`

---

### Setting a Docker Service as something to proxy with Swarm Router

You add Docker `Labels` that start with `ingress` to the Docker Services that you want the Swarm Router to proxy.  

Here you can see, how we set up both an API and a UI to be proxies by the Swarm Router for [Stable Value Dev compose.yml](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fstack%2Fstable-value%2Fdev%2Fstable-value-compose.yml&version=GBmaster&_a=contents)


More information about all of the ingress variables: 
[Documentation on the Swarm Router can be found](https://tfs.rgare.net/Actuarial%20Solutions/Actuarial%20Development/_git/swarm-router)







