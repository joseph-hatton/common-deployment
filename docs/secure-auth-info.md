# SecureAuth (Oauth)

SecureAuth is used to authenticate against and is used in a few different ways.

We use it to proxy our applications and to authenticate in for automated testing.

[SecureAuth with SwarmRouter Setup](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%secure-auth-config.md&version=GBmaster&_a=contents)

[SecureAuth (Swarm Router) Workflows](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%secure-auth-workflow.md&version=GBmaster&_a=contents)

---

## Servers and Realms

RGA has multiple SecureAuth servers (one for dev and another for prod)

These servers also have multiple `Realms` configured (separation of configuration and clientIds in a single server).

* stsdev.rgare.com `server`
  * realm31 (dev and test)
* sts.rgare.com `server`
 * realm68 (uat)
 * realm69 (prod)

---

## Flows

SecureAuth is used in a few different ways.  First we have to ask the Help Desk to create a new ClientId for us to use.

A ClientID represents a single UI or UI+API or API (or automated API client).  When requesting for them to be created, you must specify one or more flow types and the additional information that's flow specific.


### Auth Code Flow `used for UIs`

This flow allows us to see if the user is valid.  If they aren't, they can log in directly through SecureAuth (not giving us their pw) and then be redirected back to our UI with a valid SecureAuth token.  Like most of the flows, the swarm-router will handle this for us.

When creating a ClientID with this flow, you must specify redirect urls, that the SecureAuth will consider valid when someone is attempting to log in using this flow/clientID.  It is the URL that SecureAuth will HTTP redirect the user to after a valid login.  You can see why they are application specific.

For instance, Stable Value dev's redirect urls include:
  * https://stable-value-dev.rgare.net/__auth/receive-auth-code
  * http://localhost:3000/__auth/receive-auth-code

With Auth Code Flow, there are two tokens in play:
  * `access token`
    * The `access token` is used to call access any SecureAuth protected resources as an `Authorization` http header with value `Bearer {access token}`. 
    * Lifetime: good for 1 hour (unless configured differently on the SecureAuth Realm)
      * the expiration that comes back with the SecureAuth response is for this `access token`
  * `refresh token`
    * The `refresh token` should not be given to the user and only used via the SwarmRouter to get another `access token` when it is time to.
    * Lifetime: The `refresh token` in SecureAuth has a max age (since the first refresh token you received after a real login (user/pass).  In nonprod realm31 and uat realm68, this is set to 144 hours (it's usually 8 hours).)
      * Once the `refresh token` chain is no longer valid, the user will need to log in again via SecureAuth's login page.



### Password Flow `used for automated testing where we need a user logged in`

We never want the ability to see a user's password.  We use this for hardcoded system users for automated testing.
We can use this flow to authenticate with a hardcoded user/pass and get a token to be used with our APIs. 

We use this flow to send a hard-coded testing user in to get a token in api-metrics.


### Client Credentials Flow `used for calling API to API w/o a user`

Our current apps do not currently call other APIs, so they are not using this flow.  This allows for us to get a token without a user associated and call other SecureAuth enabled APIs.

---





