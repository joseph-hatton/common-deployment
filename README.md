# Fast and Stable Value common deployment scripts

Since the applications share the same VMs, they also share this same deployment script.  

---

## /scripts

* /docker - Common scripts used in other Jenkins builds to perform docker commands
* /slack-notify - A js file that can be used as a post-build jenkins step to notify a slack channel of the build's outcome

## /stack

Each cluster of VMs will have a single `Scaffold` deployment and multiple `app + environment` specific deployments.

---

### NonProd Docker Swarm Stacks 

_*Note: * Currently : stldstbvldock01, stldstbvldock02 and stldstbvldock03_

The nonprod VM cluster there will be the following docker stacks:

* scaffold
* monitor
* stable-value-dev
  * [api: `container_name: "stable-value-dev_api*"`](https://monitor.rgare.net/nagioslogserver/)
  * [legacy api: `container_name: "stable-value-dev_legacy_api*"`](https://monitor.rgare.net/nagioslogserver/)
* stable-value-test
  * [api: `container_name: "stable-value-test_api*"`](https://monitor.rgare.net/nagioslogserver/)
  * [legacy api: `container_name: "stable-value-test_legacy_api*"`](https://monitor.rgare.net/nagioslogserver/)
* stable-value-uat
  * [api: `container_name: "stable-value-uat_api*"`](https://monitor.rgare.net/nagioslogserver/)
  * [legacy api: `container_name: "stable-value-uat_legacy_api*"`](https://monitor.rgare.net/nagioslogserver/)
* fast-dev
  * [web: `container_name: "fast-dev_web*"`](https://monitor.rgare.net/nagioslogserver/)
* fast-test
  * [web: `container_name: "fast-test_web*"`](https://monitor.rgare.net/nagioslogserver/)
* fast-uat
  * [web: `container_name: "fast-uat_web*"`](https://monitor.rgare.net/nagioslogserver/)

---

### Prod Docker Swarm Stacks 

_*Note: * Currently : stlpstbvldock01, stlpstbvldock02 and stlpstbvldock03_

The prod VM cluster there will be the following docker stacks:

* scaffold
* monitor
* ~stable-value-prod~ _coming soon_
* fast-prod
  * [web: `container_name: "fast-prod_web*"`](https://monitor.rgare.net/nagioslogserver/)

---

### Scaffold Deploy

The scaffold is the infrastructure setup (not including the actual Stable Value or FAST apps). 

This will include the Swarm Router (to proxy all APIs and UIs with SecureAuth (OAuth) and all of the pieces it needs to do its job.

* stack/scaffold/nonprod/scaffold-deploy.sh
  * Deploys the scaffold


Scaffold Infrastructure:

* storage - redis cluster it stores it's proxied apps information in and it caches secureauth results
  * Does not need to be backed up. It can be deleted and restarted with no problem.
* backend - a copy of the swarm router runs as a collector that finds the apps to proxy (via docker labels) and stores its findings in the redis
* router(s) - multiple copies of the swarm router running as actual proxies that accept the only exposed ports (443), runs it's Oauth proxy logic and if happy forwards traffic to the other applications deployed in the swarm that the backend found via the docker labels


[Documentation on the Swarm Router can be found](https://tfs.rgare.net/Actuarial%20Solutions/Actuarial%20Development/_git/swarm-router)

---

### Secure Auth docs

* [Secure Auth General Info](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%2Fsecure-auth-info.md&version=GBmaster&_a=contents)
* [SecureAuth (Swarm Router) Config](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%2Fsecure-auth-config.md&version=GBmaster&_a=contents)
* [SecureAuth (Swarm Router) Workflows](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%2Fsecure-auth-workflow.md&version=GBmaster&_a=contents)

---

### TLS (HTTPS)

[HTTPS at the F5 Load Balancer and also all the way to our apps with self-signed certificates](https://tfs.rgare.net/UWSolutions/uws-gfs/_git/common-deployment?path=%2Fdocs%2Fhttps.md&version=GBmaster&_a=contents)

---

### Stable Value Dev Deploy

The Stable Value application deployment for dev

* stack/stable-value/dev/stable-value-deploy.sh
  * Deploys the stable value dev containers

---

## FAQ

### Are all docker swarm nodes attached(This will list the nodes and check to see if they are up)?

```
sudo docker node ls
```

Availability should be `Active` on each

### Are all instances of each service up?

```
sudo docker stack ls
sudo docker service ls
```

### what's going on with each node for a service that's not up?

```
sudo docker service ps --no-trunc DOCKER_SERVICE_NAME_OR_ID
```

### Full restart the router

Sometimes docker will show all application instances as running, but the browser will show an error about not being able to find an ingress name.

This is usually because the swarm-router has gotten out of sync with the services that are deployed in docker.

The swarm-router is deployed in the stack called `scaffold`.

Here we will remove the swarm-router entirely...
(Once you remove the scaffold, you can go to Jenkins ALM i.e. "deply to dev" to redeploy and restart the router)
```
sudo docker stack rm scaffold
```

* then run any deploy job that also runs the scaffold-deploy.sh (basically all of our deployment Jenkins builds)
	
```
sudo docker service ls
```

* Watch it come back online
* Test the app


### Full nuke (this takes down all apps i.e. stable-value-dev)

```
sudo docker stack ls
sudo docker stack rm EACH_STACK_NAME
```

* Have Shannon restart all of the docker VMs
* Deploy a stack at a time via Jenkins
  * ie (deploy fast_dev, fast_test, fast_uat, stable_value_dev, etc...)

  ### DB automation deploymets test: 
  https://almjenkins.rgare.net/job/ActuarialSolutions/job/FAST/job/Db%20Automation/job/Generic_DB_Deploy_Jobs_Fast/job/tryDBdeployToPROD/


